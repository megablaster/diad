<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>

            <footer>
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/logo-footer.png';?>" class="img-fluid">
                            <p>La calidad, el compromiso y los buenos resultados son sello característico de nuestro trabajo.</p>
                        </div>
                        <div class="col-xl-2 offset-xl-1">
                            <h3>Navegación</h3>
                            <ul>
                                <li><a href="<?php echo home_url('/');?>">Home</a></li>
                                <li><a href="<?php echo home_url('nuestra-firma');?>">Nuestra firma</a></li>
                                <li><a href="<?php echo home_url('servicios/comunicacion-electoral');?>">Servicios</a></li>
                                <li><a href="<?php echo home_url('contacto'); ?>">Contáctanos</a></li>
                            </ul>
                        </div>
                        <div class="col-xl-2">
                            <h3>Menú</h3>
                            <ul>
                                <li><a href="<?php echo home_url('nuestra-firma');?>">Firma</a></li>
                                <li><a href="<?php echo home_url('servicios/comunicacion-electoral');?>">Servicios</a></li>
                                <li><a href="<?php echo home_url('experiencia');?>">Experiencia</a></li>
                                <li><a href="<?php echo home_url('blog');?>">Blog</a></li>
                            </ul>
                        </div>
                        <div class="col-xl-3">
                            <h3>Sigue nuestras redes</h3>
                            <ul class="social">
                                <li>
                                    <a href="https://www.facebook.com/D%C3%ADa-D-Consultores-1923983487872022" target="_blank">
                                    <i class="fab fa-facebook-f"></i>
                                    </a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/DiaDConsultores" target="_blank">
                                        <i class="fab fa-twitter"></i>
                                    </a>
                                </li>
                                <li><a href="https://instagram.com/diadconsultoresmx?utm_medium=copy_link" target="_blank"><i class="fab fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </footer>

            <div class="top-button position-fixed zi-1020">
                <a href="#to-top" class="btn btn-primary shadow"><i class="fas fa-chevron-up"></i></a>
            </div>

        </div><!-- #page -->

        <?php wp_footer(); ?>

    </body>
</html>
