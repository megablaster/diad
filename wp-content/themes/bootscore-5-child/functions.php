<?php

// style and scripts
 add_action( 'wp_enqueue_scripts', 'bootscore_5_child_enqueue_styles' );
 function bootscore_5_child_enqueue_styles() {
     
     // style.css
     wp_enqueue_style( 'cubeportfolio-css', get_stylesheet_directory_uri().'/cubeportfolio/css/cubeportfolio.min.css' ); 
     wp_enqueue_style( 'fancybox-style', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' ); 
     wp_enqueue_style( 'owl-style', get_stylesheet_directory_uri() . '/script/owl/assets/owl.carousel.min.css' ); 
     wp_enqueue_style( 'owl-theme-style', get_stylesheet_directory_uri() . '/script/owl/assets/owl.theme.default.min.css' ); 
     
     // custom.js
     wp_enqueue_script('cubeportfolio-js', get_stylesheet_directory_uri().'/cubeportfolio/js/jquery.cubeportfolio.min.js', false, '', true);
     wp_enqueue_script('fancybox-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', false, '', true);
     wp_enqueue_script('owl-js', get_stylesheet_directory_uri() . '/script/owl/owl.carousel.min.js', false, '', true);
     wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);
 } 


function my_custom_sidebar() {
    register_sidebar(
        array (
            'name' => __( 'Blog', 'bootscore' ),
            'id' => 'blog',
            'description' => __( 'Blog sidebar', 'bootscore' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );

     register_sidebar(
        array (
            'name' => __( 'Search modal', 'bootscore' ),
            'id' => 'search-modal',
            'description' => __( 'Search modal', 'bootscore' ),
            'before_widget' => '<div class="widget-content">',
            'after_widget' => "</div>",
            'before_title' => '<h3 class="widget-title">',
            'after_title' => '</h3>',
        )
    );
    
}
add_action( 'widgets_init', 'my_custom_sidebar' );