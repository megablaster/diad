<?php
	/**
	 * The header for our theme
	 *
	 * This is the template that displays all of the <head> section and everything up until <div id="content">
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package Bootscore
	 */
	
	?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/safari-pinned-tab.svg" color="#0d6efd">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <!-- Loads the internal WP jQuery. Required if a 3rd party plugin loads jQuery in header instead in footer -->
    <?php wp_enqueue_script('jquery'); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="to-top"></div>

    <div id="page" class="site">

        <div id="mobile">
            <?php if ( function_exists( 'groovy_menu' ) ) { groovy_menu(); } ?>
        </div>
        
        <div id="nav-slide-home" class="<?php echo (is_page('experiencia') || is_home() || is_single() || is_page('contacto'))?'':'active'?>">
            <div class="close"><i class="far fa-times-circle fa-2x"></i></div>
            <div class="row">
                <div class="col-xl-10 offset-xl-2">
                    <a href="<?php echo home_url('/');?>">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo.png';?>" class="logo img-fluid">
                    </a>
                </div>
            </div>
            <ul class="top">
                <li class="<?php echo (is_front_page())? 'active':'' ?>"><a href="<?php echo home_url('/'); ?>">01. Home</a></li>
                <li class="<?php echo (is_page('nuestra-firma'))? 'active':'' ?>"><a href="<?php echo home_url('nuestra-firma'); ?>">02. Nuestra firma</a></li>
                <li class="<?php echo (is_page('servicios') || is_page('comunicacion-electoral') || is_page('comunicacion-politica-e-institucional') || is_page('gestion-electoral'))? 'active':'' ?>">
                    <a href="#" class="click-services">03. Servicios</a>
                    <ul class="sub-menu">
                        <li class="<?php echo (is_page('comunicacion-electoral'))? 'active':'' ?>"><a href="<?php echo home_url('servicios/comunicacion-electoral');?>">A. Comunicación electoral</a></li>
                        <li class="<?php echo (is_page('comunicacion-politica-e-institucional'))? 'active':'' ?>"><a href="<?php echo home_url('servicios/comunicacion-politica-e-institucional');?>">B. Comunicación política e institucional</a></li>
                        <li class="<?php echo (is_page('gestion-electoral'))? 'active':'' ?>"><a href="<?php echo home_url('servicios/gestion-electoral');?>">C. Gestión electoral</a></li>
                    </ul>
                </li>
                <li class="<?php echo (is_page('experiencia'))? 'active':'' ?>"><a href="<?php echo home_url('experiencia'); ?>">04. Experiencia</a></li>
                <li class="<?php echo (is_home())? 'active':'' ?>"><a href="<?php echo home_url('blog'); ?>">05. Blog</a></li>
                <li class="<?php echo (is_page('contacto'))? 'active':'' ?>"><a href="<?php echo home_url('contacto'); ?>">06. Contacto</a></li>
            </ul>
            <ul class="social">
                <li><a href="https://www.facebook.com/D%C3%ADa-D-Consultores-1923983487872022" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                <li><a href="https://twitter.com/DiaDConsultores" target="_blank"><i class="fab fa-twitter"></i></a></li>
                <li><a href="https://instagram.com/diadconsultoresmx?utm_medium=copy_link" target="_blank"><i class="fab fa-instagram"></i></a></li>
                <li class="block"><a href="tel:525576987789">Llámanos: +52 55 7698 7789</a></li>
            </ul>
            <div id="tab">
                <a href="<?php echo home_url('/');?>">
                <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo-mini.png';?>" class="img-fluid logo">
                </a>
                <ul>
                    <li class="click-menu">
                        <a href="#"><i class="fas fa-bars fa-2x"></i></a>
                    </li>
                    <li>
                        <a href="#modal-search" data-fancybox="new"><i class="fas fa-search fa-2x"></i></a>
                    </li>
                </ul>
            </div>
        </div>

        <div id="modal-search" style="display: none;width: 100%;max-width: 650px;border-radius: 10px;padding:10px 20px;">
            <div class="container">
                <div class="row">
                    <div class="col-xl-12">
                        <h4 style="margin:20px 0;">Realizar búsqueda:</h4>
                        <?php if (is_active_sidebar('search-modal')): ?>
                        <?php dynamic_sidebar('search-modal'); ?>
                    <?php endif ?>
                    </div>
                </div>
            </div>
        </div>

        <div id="grey"></div>

        <?php bootscore_ie_alert(); ?>
