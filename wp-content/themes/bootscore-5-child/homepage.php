<?php
/*
* Template name: Homepage
*/
?>
<?php get_header();?>
<section id="home">
	<section id="header">
		<?php echo do_shortcode('[rev_slider alias="homepage"][/rev_slider]');?>
	</section>
	<section id="second">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-6 col-lg-6">

					<div class="text">
						<div class="title-back">
							<h2>Tú eres el mensaje, <span>nosotros el medio.</span></h2>
							<h1>Estrategias <span>Ganadoras</span></h1><br>
						</div>
						<a href="#" class="btn btn-yellow">Quiero conocer más estrategias</a>
					</div>
					
				</div>

				<div class="col-xl-6 col-lg-6">
					<div id="images">
						<div class="img-1 img" style="<?php echo get_stylesheet_directory_uri().'/img/bg-1.jpg';?>">
							<div class="text">
								<p>Diseño de narrativas precisas y contundentes.</p>
							</div>
						</div>
						<div class="img-2 img" style="<?php echo get_stylesheet_directory_uri().'/img/bg-2.jpg';?>">
							<div class="text">
								<p>Seguimiento cercano y  atención individualizada al cliente.</p>
							</div>
						</div>
						<div class="img-3 img" style="<?php echo get_stylesheet_directory_uri().'/img/bg-3.jpg';?>">
							<div class="text">
								<p>Amplia experiencia en la materia, tanto adentro como fuera de gobierno.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="signature">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-5 col-lg-6 col-md-8">

					<div class="text">
						<div class="description-back">
							<h2>Nuestra firma</h2>
							<p>Somos un grupo multidisciplinario de profesionales, especializado en comunicación política y en la gerencia de procesos electorales y gobierno, con experiencia nacional e internacional.  La calidad, el compromiso y los buenos resultados son sello característico de nuestro trabajo.  Trabajamos para lograr el crecimiento y desarrollo de nuestros clientes, lo que ha permitido que juntos logremos casos de éxito.</p>
							<h3>Nuestra <span>firma</span></h3>
						</div>
					</div>

				</div>
				<div class="col-xl-5 offset-xl-1 col-lg-6 col-md-4">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/home/cube-signature.png';?>" class="img-fluid">
				</div>
			</div>
		</div>
	</section>
	<section class="pillars">
		<div class="container-fluid">
			<div class="row text-center">

				<div class="col-xl-12">

					<div class="title-back" style="text-align: left;">
						<h2>Nuestros Socios Directores</h2>
						<h1>Pilares del día d</h1>
					</div>
				<img src="<?php echo get_stylesheet_directory_uri().'/img/home/lro.png';?>" class="img-fluid">

				<div class="text">
					<h2>Rodolfo</h2>
					<h3>Socio Director Día D</h3>
				</div>

				<div class="row timeline">

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2013-2014</h4>
						<p>Jefe de Asesores del Coordinador Parlamentario en la Cámara de Diputados</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2015-2016</h4>
						<p>Consultor en Estrategia Político – Electoral</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2017</h4>
						<p>Socio Director de Día D</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2018</h4>
						<p>Reconocido por diseñar la mejor estrategia de comunicación aplicada a una campaña política</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2019</h4>
						<p>Distinción por el diseño de la mejor campaña de Informe de Gobierno</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2020</h4>
						<p>El mejor consultor gubernamental en Latinoamérica.</p>
					</div>

				</div>

				<a href="<?php echo home_url('nuestra-firma/#biography-1');?>" class="btn btn-yellow">Saber más</a>

				</div>
			</div>
		</div>
	</section>
	<section class="pillars">
		<div class="container-fluid">
			<div class="row text-center">

				<div class="col-xl-12">
				<img src="<?php echo get_stylesheet_directory_uri().'/img/home/paco.png';?>" class="img-fluid">

				<div class="text">
					<h2>Paco Valery</h2>
					<h3>Socio Estratega</h3>
				</div>

				<div class="row timeline">

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2015</h4>
						<p>Estratega en las campañas de Claudia Pavlovich en Sonora y en la campaña de Marcela Amaya en el Meta, Colombia.</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2016</h4>
						<p>Estratega de la campaña presidencial victoriosa de Lenín Moreno en Ecuador.</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2017</h4>
						<p>Estratega de la campaña nacional y presidencial del Partido Liberación Nacional en Costa Rica.</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2019</h4>
						<p>Estratega de las campañas presidenciales victoriosas, Nito Cortizo en Panamá y Alejandro Giammattei en Guatemala.</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2020</h4>
						<p>Ganador de la distinción "Rising Star" (2020) otorgado a los 15 mejores consultores políticos en el mundo por debajo de los 35 años.</p>
					</div>

					<div class="col-xl-2 col-md-4 col-sm-6">
						<div class="circle"></div>
						<h4>2020</h4>
						<p>Mejor estrategia de comunicación de crisis de COVID en Gobierno.</p>
					</div>

				</div>

				<a href="<?php echo home_url('nuestra-firma/#biography-2');?>" class="btn btn-yellow">Saber más</a>

				</div>
			</div>
		</div>
	</section>
	<section id="victory">

		<div class="container">
			<h1>Te llevamos a la victoria</h1>
			<div class="row">
				<div class="col-xl-10 offset-xl-1">
					<h2>Te llevamos a la victoria</h2>
					<p>Día D cuenta con una amplia experiencia nacional e internacional en comunicación, marketing y management político y electoral. Somos un equipo multidisciplinario capaz de entender las necesidades de nuestro clientes, diseñar estrategias efectivas y con impacto, así como dar atención y seguimiento a la gerencia de procesos electorales de manera eficaz.</p>
				</div>
			</div>
			<div class="row" id="cubes">
				<div class="col-xl-10 offset-xl-1">
					<div class="row">
						<div class="col-xl-4 col-lg-4 col-md-6">

							<a href="<?php echo home_url('servicios/comunicacion-electoral');?>" class="cube" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/cube-1.jpg';?>');">
								<div class="text">
									<h3>Comunicación ganadora</h3>
									<p>La comunicación debe ser dinámica, inclusiva y de respuesta inmediata. Además de ser fresca, renovada, con dinamismo y contundencia.</p>		
								</div>
							</a>

						</div>
						<div class="col-xl-4 col-lg-4 col-md-6">

							<a href="<?php echo home_url('servicios/comunicacion-electoral');?>" class="cube" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/cube-2.jpg';?>');">
								<div class="text">
									<h3>Elementos de la comunicación política</h3>
									<p>En la comunicación política efectiva hay tres cosas que no puedes olvidar: el mensaje, el emisor y el receptor. Es necesario definirlas.</p>		
								</div>
							</a>
						</div>
						<div class="col-xl-4 col-lg-4 col-md-6">

							<a href="<?php echo home_url('servicios/comunicacion-politica-e-institucional');?>" class="cube" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/cube-3.jpg';?>');">
								<div class="text">
									<h3>Estrategia e implementación del mensaje</h3>
									<p>Lo más importante es crear mensajes claros y sencillos, para conectar con tu audiencia (público objetivo) y asegurar sean recibidos.</p>		
								</div>
							</a>

						</div>
					</div>
				</div>
				
			</div>
		</div>
		
	</section>
	<?php get_template_part('parts/part','presencia')?>
	<section id="awards">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xl-12 p-0">
					<h2>Awards</h2>
					<p>Reconocimientos a nuestro esfuerzo y dedicación</p>
				</div>
			</div>
			<div class="row">
				<div class="col-xl-12">

					<div id="js-grid-mosaic" class="cbp cbp-l-grid-mosaic">

				       <div class="cbp-item">
				            <a href="<?php echo get_stylesheet_directory_uri().'/img/people-1.jpg';?>" class="cbp-caption cbp-lightbox" data-title="#">
				                <div class="cbp-caption-defaultWrap">
				                    <img src="<?php echo get_stylesheet_directory_uri().'/img/people-1.jpg';?>" class="img-fluid">
				                </div>
				                <div class="cbp-caption-activeWrap">
				                    <div class="cbp-l-caption-alignCenter">
				                        <div class="cbp-l-caption-body">
				                            <div class="cbp-l-caption-title">Paco Valery</div>
				                            <div class="cbp-l-caption-desc">Consultor revelación por Campaigns & Elections</div>
				                        </div>
				                    </div>
				                </div>
				            </a>
				        </div>

				        <div class="cbp-item">
				            <a href="<?php echo get_stylesheet_directory_uri().'/img/people-5.jpg';?>" class="cbp-caption cbp-lightbox" data-title="#">
				                <div class="cbp-caption-defaultWrap">
				                    <img src="<?php echo get_stylesheet_directory_uri().'/img/people-5.jpg';?>" class="img-fluid">
				                </div>
				                <div class="cbp-caption-activeWrap">
				                    <div class="cbp-l-caption-alignCenter">
				                        <div class="cbp-l-caption-body">
				                            <div class="cbp-l-caption-title">Luis Rodolfo Oropeza</div>
				                            <div class="cbp-l-caption-desc">Premio Reed Latino Awards 2018</div>
				                        </div>
				                    </div>
				                </div>
				            </a>
				        </div>

				        <div class="cbp-item">
				            <a href="<?php echo get_stylesheet_directory_uri().'/img/people-2.jpg';?>" class="cbp-caption cbp-lightbox" data-title="#">
				                <div class="cbp-caption-defaultWrap">
				                    <img src="<?php echo get_stylesheet_directory_uri().'/img/people-2.jpg';?>" class="img-fluid">
				                </div>
				                <div class="cbp-caption-activeWrap">
				                    <div class="cbp-l-caption-alignCenter">
				                        <div class="cbp-l-caption-body">
				                            <div class="cbp-l-caption-title">Equipo Día D</div>
				                            <div class="cbp-l-caption-desc">Galardonados con más de 11 premios Reed Latino por nuestro trabajo en política.</div>
				                        </div>
				                    </div>
				                </div>
				            </a>
				        </div>

				        <div class="cbp-item">
				            <a href="<?php echo get_stylesheet_directory_uri().'/img/people-6.jpg';?>" class="cbp-caption cbp-lightbox" data-title="#">
				                <div class="cbp-caption-defaultWrap">
				                    <img src="<?php echo get_stylesheet_directory_uri().'/img/people-6.jpg';?>" class="img-fluid">
				                </div>
				                <div class="cbp-caption-activeWrap">
				                    <div class="cbp-l-caption-alignCenter">
				                        <div class="cbp-l-caption-body">
				                            <div class="cbp-l-caption-title">Paco Valery</div>
				                            <div class="cbp-l-caption-desc">Ganador del Rising Star al consultor revelación del año</div>
				                        </div>
				                    </div>
				                </div>
				            </a>
				        </div>

				    </div>

					

				</div>
			</div>
		
		</div>
	</section>
	<?php get_template_part('parts/part','form')?>
</section>
<?php get_footer();?>