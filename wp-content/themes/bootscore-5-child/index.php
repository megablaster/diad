<?php get_header();?>
<div id="second"></div>
<section id="page-blog">
    <section id="header-height">
       <div class="container">
           <div class="row">
               <div class="col-xl-12 text-center">
                   <h1>Blog</h1>
               </div>
           </div>
       </div>
    </section>
    <div id="second">
        <div class="container">
            <div class="row">
                <div class="col-xl-8">
                
                    <div class="row">
                        <?php while(have_posts()): the_post() ?>

                            <div class="col-md-6">
                                <a href="<?php the_permalink(); ?>">
                                    <div class="item">
                                        <div class="img" style="background-image:url('<?php the_post_thumbnail_url();?>');"></div>
                                        <div class="text">
                                            <?php
                                                $title = get_the_title();
                                                $excerpt = get_the_excerpt();
                                                $cut = substr($title,0,100);
                                                $excerpt = substr($excerpt,0,80);
                                            ?>
                                            <h2><?php echo $excerpt.'...';?></h2>
                                            <ul>
                                                <li><?php echo get_the_date();?></li>
                                            </ul>
                                            
                                            <?php echo $cut.'...'; ?>
                                        </div>
                                        <button href="" class="btn btn-yellow">Leer más</button>
                                    </div>
                                </a>
                            </div>

                        <?php endwhile ?>
                    </div>
                </div>
                <div class="col-xl-4">

                    <?php if (is_active_sidebar('blog')): ?>
                        <?php dynamic_sidebar('blog'); ?>
                    <?php endif ?>
                    
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>