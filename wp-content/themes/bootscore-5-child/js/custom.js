 jQuery(document).ready(function ($) {

    $('.click-services').on('click',function(e){
        e.preventDefault();
        $(this).parent().toggleClass('active');
        $(this).next().toggleClass('active');
    });

    (function($, window, document, undefined) {
    'use strict';

    // init cubeportfolio
    $('#js-grid-mosaic').cubeportfolio({
            filters: '#js-filters-mosaic, #js-filters-mosaic-cat1, #js-filters-mosaic-cat2, #js-filters-mosaic-cat3, #js-filters-mosaic-cat4, #js-filters-mosaic-cat5',
            layoutMode: 'mosaic',
            sortByDimension: true,
            mediaQueries: [{
                width: 1500,
                cols: 4,
            }, {
                width: 1100,
                cols: 4,
            }, {
                width: 800,
                cols: 3,
            }, {
                width: 480,
                cols: 2,
                options: {
                    caption: '',
                    gapHorizontal: 15,
                    gapVertical: 15,
                }
            }],
            defaultFilter: '*',
            animationType: 'quicksand',
            gapHorizontal: 0,
            gapVertical: 0,
            gridAdjustment: 'responsive',
            caption: 'zoom',
            displayType: 'sequentially',
            displayTypeSpeed: 100,

            // lightbox
            lightboxDelegate: '.cbp-lightbox',
            lightboxGallery: true,
            lightboxTitleSrc: 'data-title',
            lightboxCounter: '<div class="cbp-popup-lightbox-counter">{{current}} of {{total}}</div>',

            plugins: {
                loadMore: {
                    element: '#js-loadMore-mosaic',
                    action: 'click',
                    loadItems: 3,
                }
            },
        });
    })(jQuery, window, document);

    //Add Scroll
    $(window).scroll(function() {    
        var scroll = $(window).scrollTop();
        if (scroll >= 30) {
            $("#nav-slide-home").removeClass("active");
        } else {
            $("#nav-slide-home").addClass("active");
        }
    });

    var people1 = $('.owl-people-1');
    people1.owlCarousel({
        loop:false,
        margin:20,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.prev-1').click(function(e) {
        e.preventDefault();
        people1.trigger('prev.owl.carousel', [300]);
    });

    $('.next-1').click(function(e) {
        e.preventDefault();
        people1.trigger('next.owl.carousel', [300]);
    });

    var people2 = $('.owl-people-2');
    people2.owlCarousel({
        loop:false,
        margin:20,
        nav:false,
        dots:false,
        responsive:{
            0:{
                items:1
            },
            600:{
                items:1
            },
            1000:{
                items:1
            }
        }
    });

    $('.prev-2').click(function(e) {
        e.preventDefault();
        people2.trigger('prev.owl.carousel', [300]);
    });

    $('.next-2').click(function(e) {
        e.preventDefault();
        people2.trigger('next.owl.carousel', [300]);
    });

    $('[data-fancybox="new"]').fancybox();
    
    $('.click-menu').on('click',function(e){
        e.preventDefault();
        $('#grey').addClass('active');
        $('#nav-slide-home').toggleClass('active');
    });

    $('#grey').on('click',function(e){
        e.preventDefault();
        $(this).toggleClass('active');
        $('#nav-slide-home').toggleClass('active');
    });

    $("#strategic").scroll(function() {
        $('body').css( "display", "none");
    });

    $('.close').on('click',function(e){
        e.preventDefault();
        $('#grey').removeClass('active');
        $('#nav-slide-home').toggleClass('active');
    });

    $('.item-tab').on('click', function(e){
        e.preventDefault();
        var img = $(this).data('img'); 

        $('.item-tab').find('.fas').removeClass('fa-chevron-down');
        $('.item-tab').find('.fas').addClass('fa-chevron-right');
        $(this).find('.fas').addClass('fa-chevron-down');

        $('.map').css('background-image', 'url(' + img + ')');

        $('.item-tab').removeClass('active');
        $(this).addClass('active');
    });

}); // jQuery End
