<?php get_header();?>
<section id="comunication-page">
    <section id="header">
        <?php echo do_shortcode('[rev_slider alias="comunication"][/rev_slider]');?>
    </section>
    <section id="second">
        <h4>Día D marca una diferencia positiva al posicionar mensajes contundentes, frescos y que generen un vínculo con la audiencia a la que nuestros clientes desean llegar.</h4>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-10 offset-lg-1">
                    <div class="text">
                        <h2>Marcamos la diferencia</h2>
                        <p>Día D marca una diferencia positiva al posicionar mensajes contundentes, frescos y que generen un vínculo con la audiencia a la que nuestros clientes desean llegar.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="diferent">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="title-back">
                        <h2>Así marcamos la diferencia</h2>
                        <h1>Experiencia y capacidad</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/comunication/diferent-1.jpg';?>');"></div>
                        <div class="text">
                            <h3>Diseño, planeación e implementación de la estrategia</h3>
                            <p>Creamos estrategias desde las líneas de comunicación hasta el slogan.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/comunication/diferent-2.jpg';?>');"></div>
                        <div class="text">
                            <h3>Diseño e implementación de la estrategia general de mensaje</h3>
                            <p>Generamos discursos y líneas de mensaje que preparen al candidato para el debate.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/comunication/diferent-3.jpg';?>');"></div>
                        <div class="text">
                            <h3>Planeación y management de procesos</h3>
                            <p>Integración y manejo de los procesos de comunicación, al interior de la campaña.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/comunication/diferent-4.jpg';?>');"></div>
                        <div class="text">
                            <h3>Impacto de audiencias</h3>
                            <p>Selección de target y microtargets para lograr impacto en audiencias específicas.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="numbers">
        <h4>Comunicación clara<br>Comunicación clara<br>Comunicación clara<br>Comunicación clara<br>Comunicación clara<br>Comunicación clara<br>Comunicación clara<br>Comunicación clara<br>Comunicación clara</h4>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1">
                    <div class="text">
                        <p>Somos conscientes que la comunicación es esencial en todo proceso democrático, por ello, apostamos por la claridad y la fuerza de nuestras palabras. Trabajamos el diseño de narrativas, líneas de comunicación, slogans y discursos que se apeguen a las necesidades específicas de cada cliente, basadas en un estudio de las condiciones, el votante y los parámetros que entran en juego al momento del voto.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="accordions4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1 col-md-10 offset-md-1">

                <div class="accordion">

                    <input type="radio" name="select" class="accordion-select" checked />
                    <div class="accordion-title"><span>Capacitaciones y entrenamiento de medios</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/comunication/accordion-1.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Capacitaciones y entrenamiento de medios</h2>
                                <p>Capacitamos al candidato para enfrentar escenarios importantes en los medios de comunicación en elecciones.</p>
                                <h3>Capacitaciones y entrenamiento de medios</h3>
                            </div>
                        </div>
                    </div> 
                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Branding político y de gobierno</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/comunication/accordion-2.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Branding político y de gobierno</h2>
                                <p>Construcción de imagen del candidato dentro de la campaña electoral.</p>
                                <h3>Branding político y de gobierno</h3>
                            </div>
                        </div>
                    </div> 
                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Diseño y publicidad</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/comunication/accordion-3.jpg';?>');">
                       <div class="text">
                            <div class="description-back">
                                <h2>Diseño y publicidad</h2>
                                <p>Diseño y estrategia electoral dentro del periodo de campaña.</p>
                                <h3>Diseño y publicidad</h3>
                            </div>
                        </div>
                    </div>

                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Elaboración de plan de investigación de opinión pública</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/comunication/accordion-4.jpg';?>');">
                       <div class="text">
                            <div class="description-back">
                                <h2>Elaboración de plan de investigación de opinión pública</h2>
                                <p>Encuestas de análisis de intención de voto en la campaña y percepción sobre el gobernante.</p>
                                <h3>Elaboración de plan de investigación de opinión pública</h3>
                            </div>
                        </div>
                    </div>

                </div>

                </div>
            </div>
        </div>
    </section>
    <?php get_template_part('parts/part','awards');?>
    <?php get_template_part('parts/part','form');?>
</section>
<?php get_footer();?>