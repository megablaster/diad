<?php get_header();?>
<section id="comunication-page">
    <section id="header">
        <?php echo do_shortcode('[rev_slider alias="comunicacion-politica"][/rev_slider]');?>
    </section>
    <section id="second" class="bg-political">
        <h4>Somos un equipo con amplia experiencia en elaborar y fortalecer narrativas políticas e institucionales, a partir de un conocimiento profundo de los procesos internos gubernamentales.</h4>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-10 offset-lg-1">
                    <div class="text">
                        <h2>Narrativas fuertes</h2>
                        <p>Somos un equipo con amplia experiencia en elaborar y fortalecer narrativas políticas e institucionales, a partir de un conocimiento profundo de los procesos internos gubernamentales.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="diferent">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="title-back">
                        <h2>Así generamos una Narrativa fuerte</h2>
                        <h1>Estrategia y convicción</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/political/1.jpg';?>');"></div>
                        <div class="text">
                            <h3>Diseño e implementación de la estrategia de comunicación de gobierno</h3>
                            <p>Análisis del entorno actual, electoral y ejes en los que la campaña debe desarrollarse.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/political/2.jpg';?>');"></div>
                        <div class="text">
                            <h3>Creación y manejo de los protocolos de comunicación de crisis</h3>
                            <p>Prevención de riesgos durante campaña, manejo de crisis en medios y coordinación de los equipos para unificar mensaje.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/political/3.jpg';?>');"></div>
                        <div class="text">
                            <h3>Impacto de audiencias</h3>
                            <p>Análisis global de audiencias, selección de target y microtargets para lograr impacto, posicionar mensajes e incrementar aprobación.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/political/1.jpg';?>');"></div>
                        <div class="text">
                            <h3>Capacitaciones y entrenamiento de medios</h3>
                            <p>Capacitamos al gobernante para enfrentar escenarios claves en los medios de comunicación dentro de su gestión.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="numbers" class="bg-numbers-2">
        <h4>Visión estratégica</h4>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1">
                    <div class="text">
                        <p>Detectamos áreas de oportunidad y contamos con la visión para proponer estrategias discursivas que permitan dar fuerza a los mensajes de nuestros clientes.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="accordions3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1 col-md-10 offset-md-1">

                <div class="accordion">

                    <input type="radio" name="select" class="accordion-select" checked />
                    <div class="accordion-title"><span>Branding político y de gobierno</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/political/accordion-1.jpg';?>');">

                        <div class="text">
                            <div class="description-back">
                                <h2>Branding político y de gobierno</h2>
                                <p>Construcción de imagen del candidato dentro de la administración de gobierno.</p>
                                <h3>Branding político y de gobierno</h3>
                            </div>
                        </div>

                    </div> 

                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Diseño y publicidad</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/political/accordion-2.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Diseño y publicidad</h2>
                                <p>Diseño, estrategia y asesoría de gobierno dentro del periodo de gestión del gobernante.</p>
                                <h3>Diseño y publicidad</h3>
                            </div>
                        </div>
                    </div> 

                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Elaboración de plan de investigación de opinión pública</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/political/accordion-3.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Elaboración de plan de investigación de opinión pública</h2>
                                <p>Encuestas de análisis de percepción que tiene el electorado sobre el gobernante, programa de gobierno y aprobación ciudadana dentro de la comunicación institucional.</p>
                                <h3>Elaboración de plan de investigación de opinión pública</h3>
                            </div>
                        </div>
                    </div> 

                </div>

                </div>
            </div>
        </div>
    </section>
    <?php get_template_part('parts/part','awards');?>
    <?php get_template_part('parts/part','form');?>
</section>
<?php get_footer();?>