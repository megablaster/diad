<?php get_header();?>
<div id="second"></div>
<section id="page-contact">

	<section id="header">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h2>Contáctanos</h2>
				</div>
			</div>
		</div>
	</section>

	<section id="info">
		<div class="container">
			<div class="row">
				<div class="col-xl-5 col-md-10 offset-md-1">
					<iframe src="https://snazzymaps.com/embed/354889" width="100%" height="480px" style="border:none;"></iframe>
				</div>
				<div class="col-xl-6 offset-xl-1 col-md-10 offset-md-1">
					<h2>Solicitud de contacto</h2>
					<ul>
						<li><a href="mailto:info@diad.com.mx"><i class="fas fa-envelope-square"></i> info@diad.com.mx</a></li>
						<li><a href="tel:525576987789"><i class="fas fa-phone-square-alt"></i> +52 55 76987789</a></li>
						<li><i class="fas fa-globe-americas"></i> Ciudad de México.</li>
					</ul>       
					<div class="form">
						<?php echo do_shortcode('[contact-form-7 id="12" title="Contacto"]');?>
					</div>

				</div>
			</div>
		</div>
	</section>

</section>

<?php get_footer();?>