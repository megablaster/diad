<?php get_header();?>
<div id="second"></div>
<section id="page-experience">
	<section id="header">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h1><?php the_title(); ?></h1>
				</div>
			</div>
		</div>
	</section>
	<section id="info">
		<div class="container">

			<?php
				$args = array(
					'post_type' => 'experiencias',
					'posts_per_page' => -1
				);

				$q = new WP_Query($args);

				$count = 0;
			?>

			<?php while($q->have_posts()): $q->the_post() ?>

				<?php
					$cut = substr(get_the_content(),0,350);
				?>

				<?php if ($count % 2 == 1): ?>

					<div class="row item-project">
						<div class="col-md-5">
							<img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
						</div>
						<div class="col-md-5 offset-md-1">
							<div class="text">
								<h3><?php the_title();?></h3>
								<div class="line"></div>
								<div class="resume">
									<?php echo $cut;?>
								</div>
								<a href="#modal-<?php echo $post->ID;?>" data-fancybox="new" class="btn btn-yellow">Leer más</a>
							</div>
						</div>
					</div>

				<?php else: ?>

					<div class="row item-project">
						<div class="col-md-5 offset-md-1 order-xl-1 order-md-1 order-sm-2 order-2">
							<div class="text">
								<h3><?php the_title();?></h3>
								<div class="line"></div>
								<div class="resume">
									<?php echo $cut;?>
								</div>
								<a href="#modal-<?php echo $post->ID;?>" data-fancybox="new" class="btn btn-yellow">Leer más</a>
							</div>
						</div>
						<div class="col-md-5 offset-md-1 order-xl-2 order-md-2 order-sm-1 order-1">
							<img src="<?php the_post_thumbnail_url();?>" class="img-fluid">
						</div>
					</div>
				<?php endif ?>

				<div id="modal-<?php echo $post->ID;?>" style="max-width:800px;display:none;text-align: justify;">
					<h2><?php the_title();?></h2>
					<?php the_content(); ?>
				</div>

				<?php $count = $count + 1; ?>

			<?php endwhile ?>

		</div>
	</section>
</section>
<?php get_footer();?>