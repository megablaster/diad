<?php get_header();?>
<section id="comunication-page">
    <section id="header">
        <?php echo do_shortcode('[rev_slider alias="gestion-electoral"][/rev_slider]     ');?>
    </section>
    <section id="second" class="bg-electoral">
        <h4>Día D articula y coordina los diversos procesos internos dentro de la campaña electoral mediante una adecuada gerencia de procesos electorales. </h4>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-10 offset-lg-1">
                    <div class="text">
                        <h2>Gerencia de procesos electorales</h2>
                        <p>Día D articula y coordina los diversos procesos internos dentro de la campaña electoral mediante una adecuada gerencia de procesos electorales. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="diferent">
        <div class="container">
            <div class="row">
                <div class="col-xl-10">
                    <div class="title-back">
                        <h2>Garantizamos el éxito en el proceso electoral</h2>
                        <h1>Liderazgo y crecimiento</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/manager/1.jpg';?>');"></div>
                        <div class="text">
                            <h3>Diseño, planeación e implementación de la estrategia</h3>
                            <p>Creación de narrativas, líneas y estrategias con el equipo interno que sean redituables en la campaña.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/manager/2.jpg';?>');"></div>
                        <div class="text">
                            <h3>Diseño e implementación de la estrategia general de mensaje</h3>
                            <p>Trabajo coordinado con el equipo interno que haga match con la estrategia de campaña y aprobación de contenidos generales.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-lg-4 col-md-6">
                    <div class="cube">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/manager/3.jpg';?>');"></div>
                        <div class="text">
                            <h3>Integración y manejo de procesos de comunicación</h3>
                            <p>Dirección y coordinación de war rooms para alinear la comunicación con todos los equipos de trabajo grupal a la estrategia general de la campaña.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="numbers" class="bg-numbers-3">
        <h4>Accción puntual</h4>
        <div class="container">
            <div class="row">
                <div class="col-xl-6 offset-xl-3 col-lg-10 offset-lg-1">
                    <div class="text">
                        <p>A través de una visión panorámica de las necesidades, oportunidades, metas y objetivos definidos de manera estratégica, proponemos líneas de acción, de mensaje y de comunicación, para que los equipos involucrados en la campaña trabajen en una sola dirección y obtengan resultados favorables.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="accordions3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 col-lg-10 offset-lg-1 col-md-10 offset-md-1">

                <div class="accordion">

                    <input type="radio" name="select" class="accordion-select" checked />
                    <div class="accordion-title"><span>Branding político y de gobierno</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/manager/accordion-1.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Branding político y de gobierno</h2>
                                <p>Construcción de imagen del gobernador dentro de la administración del cargo.</p>
                                <h3>Branding político y de gobierno</h3>
                            </div>
                        </div>
                    </div> 
                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Diseño y publicidad</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/manager/accordion-2.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Diseño y publicidad</h2>
                                <p>Diseño, estrategia y asesoría de gobierno dentro del periodo de gestión del gobernante.</p>
                                <h3>Diseño y publicidad</h3>
                            </div>
                        </div>
                    </div> 
                    <input type="radio" name="select" class="accordion-select" />
                    <div class="accordion-title"><span>Elaboración de plan de investigación de opinión pública</span></div>
                    <div class="accordion-content" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/manager/accordion-3.jpg';?>');">
                        <div class="text">
                            <div class="description-back">
                                <h2>Elaboración de plan de investigación de opinión pública</h2>
                                <p>Encuestas de análisis, aceptación o rechazo del candidato para valorar la confianza y conocer intención del voto.</p>
                                <h3>Elaboración de plan de investigación de opinión pública</h3>
                            </div>
                        </div>
                    </div>
                </div>

                </div>
            </div>
        </div>
    </section>
    <?php get_template_part('parts/part','awards');?>
    <?php get_template_part('parts/part','form');?>
</section>
<?php get_footer();?>