<?php get_header();?>

<section id="signature-page">
    <section id="header">
        <?php echo do_shortcode('[rev_slider alias="signature"][/rev_slider]');?>
    </section>
    <section id="second">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-8">
                    <div class="text">
                        <div class="description-back">
                            <h2>Nuestra firma</h2>
                            <p>Somos un grupo multidisciplinario de profesionales, especializado en comunicación política y en la gerencia de procesos electorales y gobierno, con experiencia nacional e internacional.</p> 
                            <p>La calidad, el compromiso y los buenos resultados son sello característico de nuestro trabajo.</p>
                            <p>Trabajamos para lograr el crecimiento y desarrollo de nuestros clientes, lo que ha permitido que juntos logremos casos de éxito.</p>
                            <h3>Nuestra <span>firma</span></h3>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 offset-xl-1 col-md-4">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/men-1.png';?>" class="img-fluid">
                </div>
            </div>
        </div>
    </section>
    <section id="history">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="title-back">
                        <h2>Nuestra trayectoria</h2>
                        <h1>Conoce nuestra historia</h1>
                    </div>
                </div>
            </div>
            <div class="row" id="image">
                <div class="col-xl-3 col-md-6">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/signature-1.jpg';?>');"></div>
                        <h3>Comunicación estratégica</h3>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/signature-2.jpg';?>');"></div>
                        <h3>Comunicación política</h3>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/signature-3.jpg';?>');"></div>
                        <h3>Manejo de crisis</h3>
                    </div>
                    <div class="col-xl-3 col-md-6">
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/signature-4.jpg';?>');"></div>
                        <h3>Consultoría política</h3>
                    </div>
                </div>
        </div>
    </section>
    <section class="biography" id="biography-1" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/signature/bg-people-1.jpg';?>');">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/people-1.png';?>" class="img-fluid people">
                </div>
                <div class="col-xl-3 col-lg-6 col-md-4">
                    <div class="text" style="text-align: left;">
                        <h2>Luis Rodolfo Oropeza</h2>
                        <p>Socio Director Día D</p>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-6 col-md-8">

                    <ul class="arrows">
                        <li class="circle"><a href="#" class="prev-1"><i class="fas fa-angle-left"></i></a></li>
                        <li class="circle"><a href="#" class="next-1"><i class="fas fa-angle-right"></i></a></li>
                    </ul>

                    <div class="owl-people">
                        <div class="cube">
                            <h2>Biografía</h2>
                            <div class="owl-carousel owl-theme owl-people-1">
                                <div class="item">
                                   
                                    <p>Cuenta con más de 17 años de experiencia en la planeación y desarrollo de campañas políticas y de gobierno, enfocadas a mejorar el desempeño de la comunicación estratégica, opinión pública, mensaje e Imagen.</p>
                                    <p>Ha desarrollado una importante carrera como Estratega de candidatos presidenciales, gobernadores, alcaldes, dirigentes de partido y diversas autoridades en todos los niveles de gobierno.</p>
        
                                  
                                </div>

                                <div class="item">
                                   
                                  <p>Estudió Ciencia Política en el Instituto Tecnológico Autónomo de México. Cuenta con cursos de especialización en Marketing Político por la Universidad George Washington y estudios de Políticas Públicas Aplicadas a Latinoamérica, por la Universidad de Harvard. Ha cursado diplomados de Política y Buen Gobierno, en el Tecnológico de Monterrey y de Análisis Político, en el Centro de Investigación y Docencia Económica.</p>
                                  
                                </div>

                                <div class="item">
                                  
                                    <p>Destaca dentro de su semblanza, su participación en el equipo estratégico del Presidente Felipe Calderón, así como su gestión y manejo de los equipos de líderes en el Congreso Mexicano y en el Partido Acción Nacional.</p>
                                    <p>Es comentarista político en El Financiero Bloomberg, ForoTV, Noticias MVS e Ibero 90.9.</p>
        
                                </div>

                                <div class="item">
                                    <p>En el 2016 resultó ganador del Reed Latino como Consultor Revelación. Ese año, también ganó el premio al Mejor <strong>Manejo de Crisis Electoral</strong>, por su trabajo en las campañas de gobernador en los estados de Aguascalientes y Durango.</p>
                                    <p>En el 2018, fue reconocido por diseñar la mejor estrategia de comunicación aplicada a una campaña política.</p> 
                                    
                                  
                                </div>

                                <div class="item">
                                    <p>Los últimos dos años, ha obtenido la distinción por el diseño de la mejor campaña de Informe de Gobierno y en 2020, como el mejor consultor gubernamental en Latinoamérica.</p> 
                                    
                                    <a href="#form" class="btn btn-yellow">Construyamos realidades juntos</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="biography" id="biography-2" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/signature/bg-people-2.jpg';?>');">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-5 col-lg-6 col-md-8 order-xl-1 order-lg-1 order-sm-2 order-2">

                    <ul class="arrows">
                        <li class="circle"><a href="#" class="prev-2"><i class="fas fa-angle-left"></i></a></li>
                        <li class="circle"><a href="#" class="next-2"><i class="fas fa-angle-right"></i></a></li>
                    </ul>

                    <div class="owl-people">
                        <div class="cube">
                            <h2>Biografía</h2>
                            <div class="owl-carousel owl-theme owl-people-2">
                                <div class="item">
                                    <p>Ganador de la distinción "Rising Star" (2020) otorgado a los 15 mejores consultores políticos en el mundo (James Carville, Ben Grinsberg lo recibieron en su momento).</p>
                                    <p>Paco Valery, es uno de los perfiles más exitosos de su generación entre los profesionales que lideran el mundo de la consultoría política.</p>
                                </div>

                                <div class="item">
                                    <p>Premiado como Consultor Revelación por la prestigiosa revista Campaigns & Elections (2019) y considerado por la Washington Academy of Political Arts and Science como uno de los 100 profesionales más influyentes en el mundo de la comunicación política (2017) y uno de los 15 mejores consultores en el mundo (2016).</p>
                                </div>

                                <div class="item">
                                    <p>Se desempeñó como estratega presidencial de Lenin Moreno en Ecuador, Nito Cortizo en Panamá y Alejandro Giammattei en Guatemala, llevándolos a la presidencia, dirigió además la campaña nacional del PLN en Costa Rica, convirtiéndolo en la primera fuerza política de la Asamblea Legislativa.</p>
                                </div>

                                <div class="item">
                                    <p>Ha asesorado campañas y equipos de gobierno exitosamente en: Venezuela, Ecuador, Colombia, Argentina, Panamá, Costa Rica, Guatemala y México.</p>
                                    <p>Se ha desempeñado además como investigador electoral para procesos políticos desarrollados en España, Honduras y El Salvador.</p>
                                    <p>Por su desempeño, las campañas que ha dirigido estratégicamente, han recibido múltiples premios:</p>
                                </div>

                                <div class="item">
                                    <p>
                                        Mejor campaña electoral del año.<br>
                                        Mejor manejo de crisis electoral.<br>
                                        Mejor Spot.<br>
                                        Mejor estrategia en Redes Sociales (Reed Awards Latinos).<br>
                                        Mejor Spot (Napolitan Victory Awards).<br>
                                        Mejor uso creativo de datos (Polaris Award - European Association of Political Consultants).
                                    </p>
                                   <a href="#form" class="btn btn-yellow">Construyamos realidades juntos</a>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-6 col-md-4 order-xl-1 order-lg-2 order-sm-1 order-1">
                    <div class="text" style="text-align: right;">
                        <h2>Paco Valery</h2>
                        <p>Socio Estratega</p>
                    </div>
                </div>
                <div class="col-xl-4 offset-md-8 order-sm-1">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/people-2.png';?>" class="img-fluid people">
                </div>
            </div>
        </div>
    </section>
    <section id="victory">
        
        <div id="bg-victory">
            <h2>Te llevamos a la victoria</h2>
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-5 col-md-5 col-sm-2">
                        <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/bg-victory.png';?>" class="img-fluid head">
                    </div>
                    <div class="col-xl-5 col-md-6 col-sm-8 col-xs-8 col-10 offset-1">
                        <div class="text">
                            <p>Comunicar es un acto esencial para construir democracia y dinamizar el campo de la política pública.</p>
                            <p>A nuestros clientes les brindamos la certeza de contar con un equipo experimentado, con dominio de su campo y la flexibilidad necesaria para entender y adaptarse al entorno político y social.</p>
                            <p>Nuestros principales atriburos son: el conocimiento, la creatividad, el compromiso, el dinamismo, la frescura y la contundencia en nuestros mensajes y estrategias.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
    <section id="number">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-sm-6">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/icon-1.png';?>" class="img-fluid">
                    <h2>22</h2>
                    <h4>Galardones <span>recibidos</span></h4> 
                </div>
                <div class="col-xl-4 col-md-4 col-sm-6">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/icon-2.png';?>" class="img-fluid">
                    <h2>127</h2>
                    <h4>Ciudades <span>Trabajadas</span></h4> 
                </div>
                <div class="col-xl-4 col-md-4 col-sm-6">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/signature/icon-3.png';?>" class="img-fluid">
                    <h2>6 <span>países</span></h2>
                    <h4>Con experiencia <span>ganadora</span></h4> 
                </div>
            </div>
        </div>
    </section>
    <section id="info">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-6">
                    <div class="cube">
                        <h3>Comunicación <span>electoral</span></h3>
                        <div class="line"></div>
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/sig-1.jpg';?>');"></div>
                        <p>Innovamos los mensajes dentro de la comunicación electoral, creamos estrategias desde las líneas de comunicación hasta el slogan de campaña.</p>
                        <a href="<?php echo home_url('comunicacion-electoral');?>" class="btn btn-yellow">Llévame al triunfo</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="cube">
                        <h3>Comunicación política <span>e institucional</span></h3>
                        <div class="line"></div>
                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/sig-2.jpg';?>');"></div>
                        <p>Diseñamos e implementamos la estrategia de comunicación de gobierno a través del análisis del entorno actual y ejecutamos las mejores estrategias institucionales.</p>
                        <a href="<?php echo home_url('comunicacion-politica-e-institucional');?>" class="btn btn-yellow">Llévame al triunfo</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-6">
                    <div class="cube">
                        <h3>Gestión <span>electoral</span></h3>
                        <div class="line"></div>
                       <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/signature/sig-3.jpg';?>');"></div>
                        <p>Trabajamos las narrativas, líneas y estrategias previamente creadas por el equipo interno para darles forma para que sea redituable la campaña.</p>
                        <a href="<?php echo home_url('gestion-electoral');?>" class="btn btn-yellow">Llévame al triunfo</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php get_template_part('parts/part','awards')?>
    <?php get_template_part('parts/part','presencia')?>
    <?php get_template_part('parts/part','form')?>
</section>

<?php get_footer();?>