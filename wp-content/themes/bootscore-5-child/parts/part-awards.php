<section id="awards-block">
	<div class="container">
		<div class="container">
			<div class="row">
				<div class="col-xl-12">
					<h2>Awards</h2>
				</div>
			</div>
		</div>
	</div>
	<section id="awards-sub">
		<div class="container">
			<div class="row">
			<div class="col-xl-4 offset-xl-4 col-sm-6">
				<ul class="line-gold">
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-1.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9 p-0">
								<div class="text">
									<h4>Mejor manejo de crisis electoral</h4>
									<h6>(Reed Latino 2016)</h6>
								</div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-1.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Mejor manejo de crisis electoral</h4>
									<h6>(Reed Latino 2016)</h6>
						        </div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-4.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Mejor campaña del Año</h4>
									<h6>(Napolitan Victory Awards 2016)</h6>
						        </div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-4.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Líder Emergente, entre los 15 mejores consultores del mundo por debajo de los 30 años</h4>
							        <h6>(Napolitan Victory Awards 2016)</h6>
						        </div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-2.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Mejor uso creativos de Datos</h4>
									<h6>(Polaris Award 2017)</h6>
						        </div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-1.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Mejor Estrategia de Comunicación aplicada a campaña electoral</h4>
							        <h6>(Runner Latino 2018)</h6>
						        </div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="col-xl-4 col-sm-6">
				<ul class="line-gold">
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/gold.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Consultor Revelación, sin importar la edad</h4>
						            <h5>(Reed Latino 2019)</h5>
								</div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/gold.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Mejor Campaña Electoral</h4>
						            <h6>(Reed Latino 2019)</h6>
								</div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-3.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Best Overall Presidential Campaign (Mejor campaña del mundo fuera de Estados Unidos)</h4>
						            <h6>(Reed Awards 2020)</h6>
								</div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-3.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Mejor estrategia de comunicación de Gobierno de Crisis para enfrentar el COVID-19</h4>
							        <h6>(Reed Latino 2020)</h6>
								</div>
							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-3.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Estratega Gubernamental del Año</h4>
							        <h6>(Reed Latino 2020)</h6>	
								</div>

							</div>
						</div>
					</li>
					<li class="item">
						<div class="row">
							<div class="col-lg-3">
								<img src="<?php echo get_stylesheet_directory_uri().'/img/icon-3.png';?>" class="img-fluid">
							</div>
							<div class="col-lg-9">
								<div class="text">
									<h4>Rising Star Latinoamericano</h4>
							        <h6>(Rising Star 2020)</h6>
								</div>
							</div>
						</div>
					</li>
				</ul>
			</div>
			</div>
		</div>
	</section>
</section>