<section id="form">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 offset-xl-1">
                
                <div class="title-back">
                    <h2>Somos un grupo especializado en comunicación política y electoral.</h2>
                    <h1>Contacto</h1>
                </div>

                <div class="form text-center">

                    <?php echo do_shortcode('[contact-form-7 id="76" title="Contacto footer"]');?>

                </div>

            </div>
        </div>
    </div>
</section>