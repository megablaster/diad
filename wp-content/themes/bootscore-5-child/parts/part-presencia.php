<section id="presencia">

    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <h2>Presencia día d</h2>
                <div class="map" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/world.png';?>');">
                    <ul>
                        <li>México</li>
                        <li>Panama</li>
                        <li>España</li>
                        <li>Costa rica</li>
                    </ul>
                    <div class="row">
                        <div class="col-xl-12">
                            <h3>Presencia Día D</h3>
                        </div>
                        <div class="col-xl-5">

                            <div id="tabs">
                                <div class="item-tab" data-img="<?php echo get_stylesheet_directory_uri().'/img/home/mexico-2.png';?>">
                                    <h4><div class="circle"><i class="fas fa-chevron-right"></i></div> <span>México</span></h4>
                                    <p>Aguascalientes, Durango, Nuevo León, Puebla, Quintana Roo, Coahuila, Guanajuato, Ciudad de México, Sinaloa, Querétaro, Michoacán, Nuevo león, Baja California Sur, Campeche</p>
                                </div>

                                <div class="item-tab" data-img="<?php echo get_stylesheet_directory_uri().'/img/home/panama.png';?>">
                                    <h4><div class="circle"><i class="fas fa-chevron-right"></i></div> <span>Panamá</span></h4>
                                    <p>Experiencia en procesos electorales y de gobierno.</p>
                                </div>

                                <div class="item-tab" data-img="<?php echo get_stylesheet_directory_uri().'/img/home/espana.png';?>">
                                    <h4><div class="circle"><i class="fas fa-chevron-right"></i></div> <span>España</span></h4>
                                    <p>Experiencia en procesos electorales y de gobierno.</p>
                                </div>

                                <div class="item-tab" data-img="<?php echo get_stylesheet_directory_uri().'/img/home/costa.png';?>">
                                    <h4><div class="circle"><i class="fas fa-chevron-right"></i></div> <span>Costa Rica</span></h4>
                                    <p>Experiencia en procesos electorales y de gobierno.</p>
                                </div>

                            </div>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

</section>