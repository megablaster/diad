<?php get_header(); the_post();?>
<section id="page-blog">
    <div id="second" style="padding:0;"></div>
    <section id="header-height" style="background-image:url('<?php the_post_thumbnail_url();?>');">
       <div class="container">
           <div class="row">
               <div class="col-xl-12 text-center">
                   <h1><?php the_title();?></h1>
               </div>
           </div>
       </div>
    </section>
    <div id="second" style="padding:40px 0;" class="single">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                
                    <div class="item" style="background: transparent;border:0;">
                        <div class="text">
                            <ul>
                                <li><?php echo get_the_date();?></li>
                            </ul>
                            <?php the_content();?>
                        </div>
                    </div>

                    
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>